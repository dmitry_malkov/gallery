<?php

class ChangeImage
{
    private $_image;
    private $_width;
    private $_height;
    private $_imageResized;
    private $_extension;

    public function __construct($fileName)
    {
        $this->_image = $this->openImage($fileName);

        $this->_width = imagesx($this->_image);
        $this->_height = imagesy($this->_image);
    }

    private function openImage($file)
    {
        $this->_extension = strtolower(strrchr($file, '.'));

        switch ($this->_extension) {
            case '.jpg':
            case '.jpeg':
                $img = @imagecreatefromjpeg($file);
                break;
            case '.gif':
                $img = @imagecreatefromgif($file);
                break;
            case '.png':
                $img = @imagecreatefrompng($file);
                break;
            default:
                $img = false;
                break;
        }
        return $img;
    }

    public function resizeImage($newWidth, $newHeight, $option = "auto")
    {

        $optionArray = $this->getDimensions($newWidth, $newHeight, strtolower($option));

        $optimalWidth = $optionArray['optimalWidth'];
        $optimalHeight = $optionArray['optimalHeight'];

        $this->_imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
        imagecopyresampled(
            $this->_imageResized,
            $this->_image,
            0,
            0,
            0,
            0,
            $optimalWidth,
            $optimalHeight,
            $this->_width,
            $this->_height
        );

        if ($option == 'crop') {
            $this->crop($optimalWidth, $optimalHeight, $newWidth, $newHeight);
        }
    }

    private function getDimensions($newWidth, $newHeight, $option)
    {
        switch ($option) {
            case 'exact':
                $optimalWidth = $newWidth;
                $optimalHeight = $newHeight;
                break;
            case 'portrait':
                $optimalWidth = $this->getSizeByFixedHeight($newHeight);
                $optimalHeight = $newHeight;
                break;
            case 'landscape':
                $optimalWidth = $newWidth;
                $optimalHeight = $this->getSizeByFixedWidth($newWidth);
                break;
            case 'auto':
                $optionArray = $this->getSizeByAuto($newWidth, $newHeight);
                $optimalWidth = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];
                break;
            case 'crop':
                $optionArray = $this->getOptimalCrop($newWidth, $newHeight);
                $optimalWidth = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];
                break;
        }
        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    private function getSizeByFixedHeight($newHeight)
    {
        $ratio = $this->_width / $this->_height;
        $newWidth = $newHeight * $ratio;
        return $newWidth;
    }

    private function getSizeByFixedWidth($newWidth)
    {
        $ratio = $this->_height / $this->_width;
        $newHeight = $newWidth * $ratio;
        return $newHeight;
    }

    private function getSizeByAuto($newWidth, $newHeight)
    {
        if ($this->_height < $this->_width) // *** Изображение является пейзажом
        {
            $optimalWidth = $newWidth;
            $optimalHeight = $this->getSizeByFixedWidth($newWidth);
        } elseif ($this->_height > $this->_width) // *** Изображение является портретом
        {
            $optimalWidth = $this->getSizeByFixedHeight($newHeight);
            $optimalHeight = $newHeight;
        } else // *** Квадрат
        {
            if ($newHeight < $newWidth) {
                $optimalWidth = $newWidth;
                $optimalHeight = $this->getSizeByFixedWidth($newWidth);
            } else {
                if ($newHeight > $newWidth) {
                    $optimalWidth = $this->getSizeByFixedHeight($newHeight);
                    $optimalHeight = $newHeight;
                } else {
                    // *** Квадрат будет изменен на квадрат
                    $optimalWidth = $newWidth;
                    $optimalHeight = $newHeight;
                }
            }
        }

        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    private function getOptimalCrop($newWidth, $newHeight)
    {

        $heightRatio = $this->_height / $newHeight;
        $widthRatio = $this->_width / $newWidth;

        if ($heightRatio < $widthRatio) {
            $optimalRatio = $heightRatio;
        } else {
            $optimalRatio = $widthRatio;
        }

        $optimalHeight = $this->_height / $optimalRatio;
        $optimalWidth = $this->_width / $optimalRatio;

        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight)
    {
        $cropStartX = ($optimalWidth / 2) - ($newWidth / 2);
        $cropStartY = ($optimalHeight / 2) - ($newHeight / 2);

        $crop = $this->_imageResized;
        //imagedestroy($this->imageResized);

        // *** Теперь обрезаем от центра до указанного размера
        $this->_imageResized = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled(
            $this->_imageResized,
            $crop,
            0,
            0,
            $cropStartX,
            $cropStartY,
            $newWidth,
            $newHeight,
            $newWidth,
            $newHeight
        );

    }

    public function saveImage($savePath, $imageQuality = "100")
    {
        $extension = $this->_extension;

        switch ($extension) {
            case '.jpg':
            case '.jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($this->_imageResized, $savePath, $imageQuality);
                }
                break;

            case '.gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($this->_imageResized, $savePath);
                }
                break;

            case '.png':
                // *** Переводим шкалу качества с 0 - 100 в 0 - 9
                $scaleQuality = round(($imageQuality / 100) * 9);

                // *** Инвертируем качество.
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                    imagepng($this->_imageResized, $savePath, $invertScaleQuality);
                }
                break;

            default:
                // *** Нет расширение - не сохраняем.
                break;
        }
        // *** Освобождаем память, уничтожая переменную с изображением
        imagedestroy($this->_imageResized);
    }
}