<?php
return array(
    'user' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'User',
        'bizRule' => null,
        'data' => null
    ),
    'admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            'user',         // позволим админу всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
);