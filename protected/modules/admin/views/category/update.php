<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->menu = array(
    array('label' => 'Журнал категории', 'url' => array('index')),
    array('label' => 'Создать категорию', 'url' => array('create')),
    array('label' => 'Просмотр', 'url' => array('view', 'id' => $model->categoryID)),

);
?>

    <h1>Изменить каегорию <?php echo $model->categoryID; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>