<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs = array(
    'Categories' => array('index'),
    $model->title,
);

$this->menu = array(
    array('label' => 'Журнал категории', 'url' => array('index')),
    array('label' => 'Создать категорию', 'url' => array('create')),
    array('label' => 'Изменить каегорию', 'url' => array('update', 'id' => $model->categoryID)),
    array(
        'label' => 'Удалить каегорию',
        'url' => '#',
        'linkOptions' => array(
            'submit' => array('delete', 'id' => $model->categoryID),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ),
);
?>

<h1>Просмотр #<?php echo $model->categoryID; ?></h1>

<?php $this->widget(
    'zii.widgets.CDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'categoryID',
            'title',
            'position',
        ),
    )
); ?>
