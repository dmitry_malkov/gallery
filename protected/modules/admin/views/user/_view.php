<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('userID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->userID), array('view', 'id'=>$data->userID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fullname')); ?>:</b>
	<?php echo CHtml::encode($data->fullname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login')); ?>:</b>
	<?php echo CHtml::encode($data->login); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('passw')); ?>:</b>
	<?php echo CHtml::encode($data->passw); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tm')); ?>:</b>
	<?php echo CHtml::encode($data->tm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roleID')); ?>:</b>
	<?php echo CHtml::encode($data->roleID); ?>
	<br />


</div>